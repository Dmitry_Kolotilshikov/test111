import { Component } from '@angular/core';
import { Dish } from 'src/app/models/dish.model';

@Component({
  selector: 'app-menu-page',
  templateUrl: './menu.component.html',
  styleUrls: ['./menu.component.scss']
})

export class MenuPageComponent {
  public dishes: Dish[] = [
    {
      id: 0,
      name: 'Котлеты',
      description: '',
      picture: '../assets/image/1.jpg',
      price: 3.5
    },
    {
      id: 1,
      name: 'Фасоль',
      description: '',
      picture: '../assets/image/2.jpg',
      price: 4.5
    },
    {
      id: 2,
      name: 'Пицца',
      description: '',
      picture: '../assets/image/3.jpg',
      price: 5.5
    },
    {
      id: 3,
      name: 'Капуста',
      description: '',
      picture: '../assets/image/4.jpg',
      price: 6.5
    },
    {
      id: 4,
      name: 'Макароны',
      description: '',
      picture: '../assets/image/5.jpg',
      price: 5.5
    },
    {
      id: 5,
      name: 'Каша овсяная',
      description: '',
      picture: '../assets/image/6.jpg',
      price: 1.5
    },
    {
      id: 6,
      name: 'Рис обыкновенный',
      description: '',
      picture: '../assets/image/7.jpg',
      price: 2.5
    }
  ];

  public order: Dish[] = [];

  public isShowModal: boolean;
  public modalData: Dish;

  constructor() { }

  public onSelect(id: number) {
    const item: Dish = this.dishes.find(dish => dish.id === id);
    this.order.push(item);
    console.log(this.order);
  }

  public get isHaveOrder(): boolean {
    return this.order.length > 0;
  }

  public showDescription(dish: Dish): void {
    this.modalData = dish;
    this.isShowModal = true;
  }

  public onModalClose(): void {
    this.isShowModal = false;
    this.modalData = null;
  }

  public onDeleteItem(item: Dish): void {
    const index = this.order.indexOf(item);
    this.order.splice(index, 1);
  }
}

import { NgModule } from "@angular/core";
import { RouterModule, Route } from "@angular/router";
import { MenuPageComponent } from './pages/menu/menu.component';
import { ChannelsPageComponent } from './pages/channels/channels.component';
import { AboutPageComponent } from './pages/about/about.component';


const routes: Route[] = [
  {
    path: '',
    redirectTo: 'menu',
    pathMatch: 'full'
  },
  {
    path: 'menu',
    component: MenuPageComponent
  },
  {
    path: 'channels',
    component: ChannelsPageComponent
  },
  {
    path: 'about',
    component: AboutPageComponent
  }
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes)
  ],
  exports: [RouterModule]
})


export class RoutingModule { }

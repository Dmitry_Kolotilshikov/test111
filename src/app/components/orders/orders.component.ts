import { Component, Input, Output, EventEmitter, OnInit, OnDestroy } from '@angular/core';
import { Dish } from 'src/app/models/dish.model';

@Component({
  selector: 'app-orders',
  templateUrl: './orders.component.html',
  styleUrls: ['./orders.component.scss']
})
export class OrdersComponent implements OnInit, OnDestroy {

  @Input() order: Dish[];

  @Output() deleteEvent = new EventEmitter<Dish>();

  constructor() { }

  ngOnInit() {
    console.log('Order component создан');
  }
  ngOnDestroy() {
    console.log('Order component удален');
  }

  public get summ(): number {
    let total = 0;
    this.order.forEach(item => {
      total += item.price;
    });
    return total;
  }

  public deleteItem(item: Dish): void {
    this.deleteEvent.emit(item);
  }
}

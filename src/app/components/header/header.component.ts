import { Component } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})

export class HeaderComponent {
  constructor(private router: Router) { }

  public redirectToChannels(): void {
    this.router.navigate(['channels']);
  }

  public get isChannelsActive(): boolean {
    const url = this.router.url; //'http://localhost: 4200/channels or menu
    return url.includes('channels');
  }
}

import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { OrdersComponent } from './components/orders/orders.component';
import { DishComponent } from './components/dish/dish.component';
import { ModalComponent } from './components/modal/modal.component';
import { HeaderComponent } from './components/header/header.component';
import { MenuPageComponent } from './pages/menu/menu.component';
import { RoutingModule } from './routing.module';
import { ChannelsPageComponent } from './pages/channels/channels.component';
import { AboutPageComponent } from './pages/about/about.component';

@NgModule({
  declarations: [
    AppComponent,
    DishComponent,
    OrdersComponent,
    ModalComponent,
    HeaderComponent,
    MenuPageComponent,
    ChannelsPageComponent,
    AboutPageComponent
  ],
  imports: [
    BrowserModule,
    RoutingModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
